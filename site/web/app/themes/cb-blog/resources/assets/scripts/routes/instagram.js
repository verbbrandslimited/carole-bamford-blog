export default {
    init() {

        let loading = false;
        let instagramArchive = $('#feed');

        $('.instagram-feed a.load-more').click(function (e) {
            e.preventDefault();
            let $this = $(this);
            let href = $this.attr('href');
            if (!loading) {
                loading = true;
                instagramArchive.addClass('loading');
                $.ajax({
                    type: "GET",
                    url: href,
                    success: function (data) {
                        loading = false;
                        instagramArchive.removeClass('loading');
                        if (data.data) {
                            $.each(data.data, function (k, v) {
                                instagramArchive.find('.feed__row','').append('<a href="' + v.link + '" class="feed__row__block" style="background: url(' + v.images.standard_resolution.url + ');"></a>');
                            })
                        }
                        if (data.pagination.next_url) {
                            $this.attr('href', data.pagination.next_url);
                        } else {
                            $this.slideUp();
                        }
                    },
                    error: function () {
                        $this.slideUp();
                    },
                })
            }

        })



    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};

$(document).ready(function(){
    // Initiate Slider with parameters
    var owl =  $('.owl-carousel');
    owl.owlCarousel({
        items: 1,
        loop:false,
        autoplay:true,
        nav:true,
        dots:false,
        autoplayTimeout:8000,
        navText: ["&nbsp;" , "&nbsp;"],
        smartSpeed: 1000,
    });

    // Set Slider nav defaults.
    let activeItem = 1;
    $('.slider-nav__item#1').addClass('active');
    let i;

    // Get number of items in the slider nav.
    let items = $('.slider-nav').children().length;

    //Set default changeBy value to be 0. So that the slider nav still
    // updates when the slider changes automatically.
    let changeBy = 0;

    // Go to specific slide when slider nav item clicked.
    function sliderNav( i ) {
        return function() {

            // Works out how many slides to increment the nav by, by
            // taking the id of the next slide from the id of the current
            let nextSlide = $(this).attr('id');
            let currentSlide = $('.slider-nav__item.active').attr('id');
            changeBy = nextSlide - currentSlide;

            // Removes the active class from current nav item
            $('.slider-nav').find('.slider-nav__item.active').removeClass('active');

            // Triggers the slide change. (the value of i is defined by
            // whichever nav item is clicked)
            $('.owl-carousel').trigger('to.owl.carousel', (i-1));

            // Adds active class to the clicked item.
            $(this).addClass('active');

            // Sets the value of activeItem to be whichever slide it is
            // now on.
            activeItem = i;
        }
    }
    // Loops through items in nav menu. Initiates slideNav function, passing
    // the current nav item as an arguement.
    for(i = 0; i <= items; i++) {
        $('.slider-nav__item#'+i).click(sliderNav( i ));
    }

    // When slider changes (either by time or navigation) activeItem is
    // incremented, changing the active state on the relevant nav item.
    owl.on('change.owl.carousel', function() {
        let i = 0;

        // changeBy is 0 by default. This if statement means that the nav will
        // still increment if it isn't clicked and the slider changes automatically
        if(changeBy === 0){
               i = (activeItem + 1);
        } else {
            i = (activeItem + changeBy);
        }

        // Removes the active class from the current menu item.
        $('.slider-nav').find('.slider-nav__item.active').removeClass('active');

        // Increments the active class
        $('.slider-nav__item#' + i).addClass('active');

        // Stops activeItem getting higher than the amount of slides that there are.
        // When it equals the totalSlides, it is reset to zero.
        let slides = $('.owl-stage').children().length;
        if(activeItem < ( slides - 1 )) {
            activeItem=i;
        } else if(activeItem >= ( slides - 1 )) {
            activeItem = 0;
        }

        // Reset the value of changeBy so that the active class can still be incremented
        // automatically after a nav click
        changeBy = 0;
    });
});	






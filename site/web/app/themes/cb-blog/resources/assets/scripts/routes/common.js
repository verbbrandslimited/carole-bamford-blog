export default {
  init() {
    $('.resp-nav__button').click(function(){
      $(this).toggleClass('active');
      $('.mobile-nav').toggleClass('active');
    });


    $('.image-changer-nav__item').click(function(){
      $('.image-changer-view').find('.active').removeClass('active');
      $('.image-changer-view__item').eq( $(this).index() ).addClass( 'active' );
      $('.image-changer-nav').find('.active').removeClass('active');
      $(this).toggleClass('active');
    });

    $('.search-button').click(function(){
      $('.search-bar').toggleClass('active');
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};

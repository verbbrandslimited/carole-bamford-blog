export default {
    init() {

        let loading = false;
        let postArchive = $('#post-archive');

        $('.see-more-archive a').click(function (e) {
            e.preventDefault();
            let $this = $(this);
            let href = $this.attr('href');
            if (!loading) {
                loading = true;
                postArchive.addClass('loading');
                $.ajax({
                    type: "GET",
                    url: href,
                    success: function (data) {
                        loading = false;
                        postArchive.removeClass('loading');

                        let posts = $(data).find('#post-archive');
                        let href = $(data).find('.see-more-archive a');

                        if(!href.length) $this.slideUp();

                        if (posts.length) {
                            postArchive.append(posts.html());
                            $this.attr('href', href.attr('href'));
                        }
                        else $this.slideUp();

                    },
                    error: function () {
                        $this.slideUp();
                    },
                })
            }

        })
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    },
};

{{--
  Template Name: Instagram Template
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php(the_post())
    @include('partials.content-insta')
    @endwhile
@endsection
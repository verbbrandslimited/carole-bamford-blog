<form role="search" method="get" class="search-form" action="<?php echo get_home_url(); ?>">
    <label>
        <span class="screen-reader-text">Search for:</span>
        <input type="search" class="search-field" placeholder="Enter Search Keywords" value="" name="s">
    </label>
</form>
<button class="search-button"></button>
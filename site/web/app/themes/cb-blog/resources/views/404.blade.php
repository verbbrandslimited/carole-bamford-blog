<?

$title = get_field('error_title', 'options');
$content = get_field('error_content', 'options');
$button = get_field('error_button_title', 'options');
$buttonLink = get_field('error_button_link', 'options');

?>

@extends('layouts.app')

@section('content')

  @if (!have_posts())
    <div class="container">

      <div class="inner-container col-lg-10">

        <div class="row">

          <div class="content-center error-col col-sm-12 col-md-6">

            <div class="error-content">

                <div class="error-title"><h1><?= $title ?></h1></div><!-- error title -->

                <div class="content"><?= $content ?></div><!-- content -->

                <span class="back-to-homepage-button"><a href="<?= $buttonLink ?>"><?= $button ?></a></span><!-- back to homepage button -->

              {{--{!! get_search_form(false) !!}--}}

            </div><!-- error content -->

          </div><!-- col-md-6 -->

          <div class="error-col col-sm-12 col-md-6">

            <img src="<?php echo get_template_directory_uri();?>/assets/images/404-background-image.jpg" />

          </div><!-- col-md-6 -->

        </div><!-- row -->

      </div><!-- col-lg-10 -->

    </div><!-- container -->
  @endif
@endsection
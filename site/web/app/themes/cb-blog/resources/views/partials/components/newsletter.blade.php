{{--@php(var_dump($newsletter))--}}
@if($newsletter['background_image']['url'])
    @php($image = $newsletter['background_image']['url'])
@else
    @php($image = get_field('newsletter_signup_image','options'))
    @php($image = $image['url'])
@endif
<section class="newsletter-signup" style="background-image:linear-gradient(rgba(0,0,0,0.1), rgba(0,0,0,0.4)), url('{{ $image }}');">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="newsletter-signup__content">
                    <p>Sign up to recieve my monthly newsletter</p>
                    <!-- Begin MailChimp Signup Form -->

                    <form action="https://carolebamford.us9.list-manage.com/subscribe/post?u=6dd4696d034fe3cc0845c8271&amp;id=efb84f41c9" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate signup-form" target="_blank" novalidate>

                        <input type="text" value="" name="MMERGE3" placeholder="Full Name..." class="signup-form__field" id="mce-MMERGE3">
                        <input type="email" value="" name="EMAIL" placeholder="Email Address..." class="required email signup-form__field" id="mce-EMAIL">

                        <div id="mce-responses" class="clear">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                        <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6dd4696d034fe3cc0845c8271_efb84f41c9" tabindex="-1" value=""></div>
                        <input type="submit" value="Sign Up" name="subscribe" id="mc-embedded-subscribe" class="signup-form__submit">

                    </form>

                    <!--End mc_embed_signup-->
                </div>
            </div>
        </div>
    </div>
    @if($newsletter['text'])
        <p class="small-text">Image Source: <a href="{{ $newsletter['text'] }}" target="_blank">{{ $newsletter['text'] }}</a></p>
    @endif
</section>
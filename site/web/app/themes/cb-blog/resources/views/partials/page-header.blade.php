<div class="page-header">
  <div class="container">
    <div class="row">
      <h1 class="section-title">{!! App::title() !!}</h1>
    </div>
  </div>
</div>

@php
  $id = get_the_ID();
  $categories = get_the_category($id);
  $category = $categories[0]->name;
  $catSlug = $categories[0]->slug;
@endphp
<article @php(post_class())>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10">
        <div class="page-header {{ $catSlug }}">
          {{--<p class="page-header__cat">{{ $category }}</p>--}}
          <h1 class="section-title individual-posts-header">{{ get_the_title() }}</h1>
          @if(in_category('recipe'))
            @include('partials.components.recipe-info')
          @endif
        </div>
        @if(have_rows('image_changer'))
          @include('partials.components.image_changer')
        @else
          @include('partials.components.single-post-featured-image')
        @endif
        <div class="entry-content row justify-content-center">
          <div class="col-md-10">
            @php(the_content())
            @if(in_category('recipe') && have_rows('ingredients'))
              @include('partials.components.ingredients')
            @endif
            @if(in_category('recipe') && have_rows('method'))
              @include('partials.components.method')
            @endif
          </div>
        </div>
        @include('partials.components.single-post-pagination')
        @include('partials.components.related-posts')
      </div>
    </div>
  </div>
  @php($newsletter = get_field('newsletter_signup'))
  @include('partials.components.newsletter')
</article>

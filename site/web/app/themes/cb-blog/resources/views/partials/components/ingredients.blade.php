<div class="ingredients-section">
    <h2 class="section-title">Ingredients</h2>
    <ul class="ingredients">
        @while(have_rows('ingredients')) @php(the_row())
            @php
                $ingredient = get_sub_field('ingredient');
            @endphp
            <li class="ingredient">{{ $ingredient }}</li>
        @endwhile
    </ul>
</div>
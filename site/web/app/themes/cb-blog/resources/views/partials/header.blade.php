<header class="global-header">
  <div class="container">
    <div class="row justify-content-center">
    	<div class="col-lg-10">
    		<div class="row justify-content-center">
		    	<div class="logo-holder col-md-4 col-10">
		    		<a class="logo" href="{{ home_url('/') }}">
						<img src="{{ get_template_directory_uri() }}/../dist/images/logo.svg" />
						<p class="cursive">A Way of Life</p>
		    		</a>
		    	</div>
		    </div>
		    @include('partials.components.nav')
	    </div>
    </div>
  </div>
</header>

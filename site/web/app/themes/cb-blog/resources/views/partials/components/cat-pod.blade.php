@php
    $image = get_the_post_thumbnail_url();
    $categories = get_the_category($post->ID);
    $catID = get_category_link($categories[0]->term_id);
    $category = $categories[0]->name;
@endphp
<div class="col-sm-6">
    <article class="cat-pod">
        <a href="@php(the_permalink())">
            <div class="cat-pod__image" style="background-image:url('{{ $image }}');"></div>
            <p class="cat-pod__cat">{{ $category }}</p>
            <div class="cat-pod__title">
                <h2>{!! wp_trim_words(get_the_title(), 5, '...') !!}</h2>
            </div>
        </a>
    </article>
</div><!--col-->

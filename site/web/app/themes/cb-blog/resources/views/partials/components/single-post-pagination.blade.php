<nav class="pagination">
    <div class="pagination__prev">
        @php(previous_post_link('%link', '<< Previous Post', true))
    </div>
    <div class="pagination__next">
        @php(next_post_link(next_post_link('%link', 'Next Post >>', true)))
    </div>
</nav>
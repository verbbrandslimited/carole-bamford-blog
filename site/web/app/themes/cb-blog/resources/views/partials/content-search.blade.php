<article @php(post_class('search-results__result'))>
  <a class="result-image" href="{{ get_permalink() }}">
    <div class="result-image__image" style="background-image:url('{{ get_the_post_thumbnail_url() }}');"></div>
  </a>
  <div class="result-content">
    <header>
      <h2 class="entry-title"><a href="{{ get_permalink() }}">{{ get_the_title() }}</a></h2>
    </header>
    <div class="entry-summary">
      @php(the_excerpt())
    </div>
  </div>
</article>

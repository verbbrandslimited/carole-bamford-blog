<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-10">
            <div class="owl-carousel owl-theme">
                {!! Slider::slides(get_sub_field('slider')) !!}
            </div>
            <div class="slider-nav">
                {!! Slider::slideNav(get_sub_field('slider')) !!}
            </div>
        </div>
    </div>
</div>
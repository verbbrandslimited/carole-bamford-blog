@php
    $image = get_the_post_thumbnail_url();
@endphp
<div class="featured-image" style="background-image:url('{{ $image }}');"></div>
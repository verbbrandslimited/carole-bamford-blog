<section class="image-changer">
    <div class="image-changer-view">
        @php($i = 1)
        @while(have_rows('image_changer'))@php(the_row())
            @php
                $imageArray = get_sub_field('image');
                $image =$imageArray['sizes']['large'];
            @endphp
            @if($i == 1)
                @php($class = 'active')
            @else
                @php($class = '')
            @endif
            <div class="image-changer-view__item {{ $class }}" style="background-image:url('{{ $image }}');" id="{{ $i }}"></div>
            @php($i++)
        @endwhile
    </div>

    <div class="image-changer-nav">
        @php($i = 1)
        @while(have_rows('image_changer'))@php(the_row())
        @php
            $imageArray = get_sub_field('image');
            $image =$imageArray['sizes']['medium'];
        @endphp
        @if($i == 1)
            @php($class = 'active')
        @else
            @php($class = '')
        @endif
        <div class="image-changer-nav__item {{ $class }}" style="background-image:url('{{ $image }}');" data-item="{{ $i }}"></div>
        @php($i++)
        @endwhile
    </div>
</section>
@php
    $cats = get_the_category();
    $args = array(
        'post_type' => 'post',
        'post__not_in' => array( get_the_ID() ),
        'posts_per_page' => 3,
        'cat' => $cats[0]->term_id,
    );
    $query = new WP_Query( $args );
@endphp
@if($query->have_posts())
    <section class="related-posts">
        <div class="row">
            <h2 class="section-title">Related Posts</h2>
        </div>

        <div class="row">
            @while($query->have_posts()) @php($query->the_post())
            @include('partials.components.cat-pod-related')
            @endwhile
        </div>
    </section>
@endif @php(wp_reset_postdata())
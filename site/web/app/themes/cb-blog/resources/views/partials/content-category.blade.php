@php( $nextLink = get_next_posts_link('see more'))
@php(  $paged = get_query_var( 'paged', 1 ))
@php global $wp_query @endphp

@include('partials.page-header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row" id="post-archive" data-query='{{ json_encode($wp_query->query) }}'>
                @while (have_posts()) @php(the_post())
                    @include('partials.components.cat-pod')
                @endwhile


                @if (!have_posts())
                    @include('partials.components.no-results')
                @endif

            </div>

            <div class="see-more-archive">{!! $nextLink !!}</div><!-- see more button -->
        </div>
    </div>
</div>
@php($term = get_queried_object())
@if(get_field('newsletter_signup', $term))
    @php
        $newsletter = get_field('newsletter_signup', $term);
    @endphp
    @include('partials.components.newsletter')
@endif
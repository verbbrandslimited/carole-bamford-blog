<div class="method-section">
    <h2 class="section-title">Method</h2>
    <ol class="steps">
        @while(have_rows('method')) @php(the_row())
        @php
            $step = get_sub_field('step');
        @endphp
        <li class="step">{{ $step }}</li>
        @endwhile
    </ol>
</div>
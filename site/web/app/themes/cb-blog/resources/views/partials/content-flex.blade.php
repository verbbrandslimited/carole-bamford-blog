@if(have_rows('content') ) 
	@while( have_rows('content') ) @php (the_row())

		@include('partials.flex-'.get_row_layout())
	
	@endwhile
@endif
<nav class="navigation">
    @if (has_nav_menu('primary_navigation'))
        <ul class="nav">
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => '', 'items_wrap' => '%3$s']) !!}
            <li class="search-button"></li>
            <div class="search-bar">
                {{ get_search_form() }}
            </div>
        </ul>
        <section class="resp-nav">
            <button class="resp-nav__button">
                <span class="line"></span>
                Menu
            </button>
            <div class="resp-nav__menu">
                {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'mobile-nav']) !!}
            </div>
        </section>
    @endif
</nav>
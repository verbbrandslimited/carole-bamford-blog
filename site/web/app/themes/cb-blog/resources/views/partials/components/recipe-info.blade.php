<div class="recipe-info">
    <p>Serves {{ get_field('serves') }}</p>
    <p>Cooking Time - {{ get_field('cooking_time') }}</p>
</div>
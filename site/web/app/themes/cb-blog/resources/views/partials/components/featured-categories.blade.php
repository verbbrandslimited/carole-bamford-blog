<section class="categories">
    <div class="container">
        <div class="row">
            <h2>Categories</h2>
        </div>
        <div class="featured-cat-container">
            <div class="row">
                @php ($terms = get_sub_field('featured_categories'))
                @if($terms)
                    @foreach($terms as $term)
                        @php
                            $image = get_field('featured_image', $term);
                        @endphp
                        <div class="featured-cat col-lg-3 col-6">
                            <a href="{{ get_term_link($term) }}">
                                <div class="featured-cat__image" style="background-image:url('{{ $image['url'] }}');"></div>
                            </a>
                            <p class="featured-cat__title"><a href="{{ get_term_link($term) }}">{{ $term->name }}</a></p>
                            <a href="{{ get_term_link($term) }}">Explore</a>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</section>

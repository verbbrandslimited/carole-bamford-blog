@php

    use App\Instagram;
    $feed = new Instagram();
    $feed = $feed->getFeed();

$insta = get_field('instagram_title', 'options');
$instaLink = get_field('instagram_url', 'options');
@endphp

<div class="page-header">
    <div class="container">
        <div class="row">
            <h1 class="section-title">{!! App::title() !!}</h1>
            <?= $insta ? "<div class='instagram-subtitle'><a href='$instaLink'>$insta</a></div>": ''?>
        </div>
    </div>
</div>


<section class="instagram-feed">

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-10">

                <div id="feed">

                    <div class="feed__row">

                        <?php foreach ($feed->data as $entry) : ?>

                        <a href="{{$entry->link}}" target="_blank" class="feed__row__block "
                             style="background: url({!! $entry->images->standard_resolution->url !!});"></a>


                        <?php  endforeach;?>

                    </div><!-- feed__row -->

                </div>

                {!! isset($feed->pagination->next_url) ? "<a class='load-more' href='{$feed->pagination->next_url}'>Load More</a>" : ''; !!}

            </div><!-- col-md-10 -->

        </div><!-- row justify-content-center -->


    </div>

</section>


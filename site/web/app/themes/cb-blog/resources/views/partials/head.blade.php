<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link href="https://fonts.googleapis.com/css?family=Italianno|Lora:400,700|Raleway:300,400,500,600,700" rel="stylesheet">
  @if( get_field('header', 'option') )
    @php( the_field('header', 'option') )
  @endif
  @php(wp_head())
</head>

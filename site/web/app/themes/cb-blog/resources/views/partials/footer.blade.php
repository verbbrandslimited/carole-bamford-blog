<?php
$contact = get_field('general_enquiries', 'options');
$contactLink = get_field('email', 'options');
$external = get_field('external_site', 'options');
$externalLink = get_field('external_site_link', 'options');
$externaltwo = get_field('external_site_2', 'options');
$externalLinktwo = get_field('external_site_link_2', 'options');
?>
<footer>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-12 col-lg-auto half-left">
                    &copy; Carole Bamford. All rights reserved.
                </div>
                <div class="col-md-12 col-lg-auto half-left">
                    <?= $contact ? "<span class='general-enquiries'><a href='mailto:$contactLink'>$contact</a></span>" : '' ?>
                    <?= $external ? "<span class='general-enquiries'><a href='$externalLink'>$external</a></span>" : '' ?>
                    <?= $externaltwo ? "<span class='general-enquiries'><a href='$$externalLinktwo'>$externaltwo</a></span>" : '' ?>
                </div>
                <div class="col-md-12 col-lg-3 half-right">
                    Design & Site By <a href="https://www.verbbrands.com" target="_blank">Verb</a>
                </div>
            </div>
        </div>
    </div>
  </div>
</footer>
@extends('layouts.app')

@section('content')
  @include('partials.page-header')
  <section class="search-results">
    @if (!have_posts())
      @include('partials.components.no-results')
    @endif
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-10">

          @while(have_posts()) @php(the_post())
            @include('partials.content-search')
          @endwhile

        </div>
      </div>
    </div>
  </section>

  {!! get_the_posts_navigation() !!}
@endsection

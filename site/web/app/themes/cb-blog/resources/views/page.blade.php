@extends('layouts.app')

@section('content')
  @while(have_posts()) @php(the_post())
    @include('partials.page-header')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                @include('partials.content-page')
            </div><!-- col-md-10 -->
        </div><!-- row justify-content-center -->
    </div><!-- Container -->
  @endwhile
@endsection

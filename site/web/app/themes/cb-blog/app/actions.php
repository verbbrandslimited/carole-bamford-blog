<?php
/**
 * Created by PhpStorm.
 * User: shahramzamani
 * Date: 11/04/2018
 * Time: 14:21
 */

namespace App;

/**
 * Load More Archive
 */
function loadMoreArchive()
{
    if (!isset($_POST['query'])) {
        wp_send_json_error('there seems to be an error');
    } else {
        if (!isset($_POST['query']['paged'])) $_POST['query']['paged'] = 1;

        $_POST['query']['paged'] = intval($_POST['query']['paged']) + 1;
        $query = new \WP_Query($_POST['query']);

        if ($query->have_posts()) :
            ob_start();
            while ($query->have_posts()) : $query->the_post();
                echo \App\template(locate_template('views/partials/components/cat-pod'));
            endwhile;
            $content = ob_get_clean();
        else:
            $content = '<li><h2>Sorry there are no more previous exhibitions</h2></li>';
        endif;


        wp_send_json_success([
            'query' => $_POST['query'],
            'more' => $query->max_num_pages !== $_POST['query']['paged'],
            'content' => $content
        ]);

    }
}


add_action("wp_ajax_load_more_archive", __NAMESPACE__ . '\\loadMoreArchive');
add_action("wp_ajax_nopriv_load_more_archive", __NAMESPACE__ . '\\loadMoreArchive');

/**
 * Font Awesome In Backend
 */
function fontawesome_dashboard()
{
    wp_enqueue_style('fontawesome', 'http:////netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css', '', '4.7.0', 'all');
}

add_action('admin_init', __NAMESPACE__ . '\\fontawesome_dashboard');

/**
 * Font Awesome In Backend
 */
function fontawesome_icon_dashboard()
{
    echo "<style type='text/css' media='screen'>
   		.dashicons-instagram:before {
   		font-family: Fontawesome !important;
   		content: '\\f16d';
     }
     	</style>";
}

add_action('admin_head', __NAMESPACE__ . '\\fontawesome_icon_dashboard');

/**
 * Instagram Page
 */
function instagramAdminPageInit()
{
    add_menu_page(
        'Instragram Settings',
        'Instragram',
        'manage_options',
        'instagram', __NAMESPACE__ . '\\instagramAdminPage', 'dashicons-instagram', 81);
}

add_action('admin_menu', __NAMESPACE__ . '\\instagramAdminPageInit');

function instagramAdminPage()
{
    $instagram = new Instagram();
    $code = isset($_GET['code']) ? $_GET['code'] : false;
    $auth = $instagram->getAuth($code);
    if (is_object($auth) || is_array($auth))
        echo "<div class='notice' style='padding: 10px; margin:20px 0; max-width:300px; text-align: center'><img style='border-radius: 50%; ' src='{$auth->user->profile_picture}' /><p>{$auth->user->username} is connected</p><a href='#'>Remove Account</a></div>";
    else
        echo "<div class='notice' style='padding: 10px; margin:20px 0;'>You currently have no Auth token. <a href='$auth'>Get Token</a></div>";
}
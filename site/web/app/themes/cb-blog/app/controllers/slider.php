<?php 

namespace App;

use Sober\Controller\Controller;

class Slider extends Controller
{
	public static function slides($posts)
	{
		$slides = '';

		wp_enqueue_script('verb/slider-init.js', asset_path('scripts/slider-init.js'), ['jquery'], null, true);

		foreach($posts as $post) {

			$categories = get_the_category($post->ID);
			$categoryID = $categories[0]->term_id;
			$category = $categories[0]->name;

			$slides .= '<div class="owl-item__slide" style="background-image:url(';
			$slides .= get_the_post_thumbnail_url($post->ID);
			$slides .= ');">';
			$slides .= "<div class='content'>";
			$slides .= "<a class='slider-link' href='";
			$slides .= get_category_link($categoryID);
			$slides .= "'>";
			$slides .= $category;
			$slides .= "</a>";
			$slides .= "<h1>";
			$slides .= $post->post_title;
			$slides .= "</h1>";
			$slides .= "<a class='cta' href='";
			$slides .= get_permalink($post->ID);
			$slides .= "'>Continue Reading</a>";
			$slides .= "</div>";
			$slides .= '</div>';
		}
		return $slides;
	}

    public static function slideNav($posts)
    {
        $slideNav = '';
        $i = 1;
        foreach($posts as $post) {

            $slideNav .= '<div class="slider-nav__item" id="';
            $slideNav .= $i;
            $slideNav .= '">';
            $slideNav .= $i;
            $slideNav .= ". ";
            $slideNav .= $post->post_title;
            $slideNav .= '</div>';

            $i++;
        }
        return $slideNav;
    }
}
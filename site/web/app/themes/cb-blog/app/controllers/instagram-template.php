<?php

namespace App;

use Andreyco\Instagram\Client;
use Andreyco\Instagram\Exception\InvalidParameterException;
use Sober\Controller\Controller;

class Instagram extends Controller
{

    private $instagram;
    private $instagramToken;
    private $instagramInfo;


    public function __construct()
    {
        try {
            $this->instagram = new Client([
                'apiKey' => 'a39d9504d62b4dbcbf5eae16acba0897',
                'apiSecret' => 'a9dc85acdc684e57952cbf519334e5bb',
                'apiCallback' => admin_url('admin.php?page=instagram'),
                'scope' => ['basic'],
            ]);

            $this->instagramInfo = get_option('instagram_token');
            $this->instagramToken = false;
            if($this->instagramInfo) {
                $token = json_decode($this->instagramInfo);
                $this->instagramToken = $token->access_token;
            }

        } catch (InvalidParameterException $e) {

        }

    }

    public function getAuth($code = false)
    {
        if (!$this->instagramInfo && $code) {
            $data = $this->instagram->getOAuthToken($code);
            update_option('instagram_token', json_encode($data));
            return $data;
        } else if ($this->instagramInfo) {
            return json_decode($this->instagramInfo);
        } else {
            return $this->instagram->getLoginUrl();
        }
    }

    public function getFeed($page = 1) {

        if(!$this->instagramInfo)
            return 'No Access token found!';
        else {
            $this->instagram->setAccessToken($this->instagramToken);
            return $this->instagram->getUserMedia($id = 'self', 17);
        }
    }
}
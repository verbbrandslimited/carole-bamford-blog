<?php
/**
 * Created by Connor Mulhall.
 * For Verb Brands.
 * Date: 13/04/2018
 * Time: 09:25
 */

require '../vendor/autoload.php';

try {
    $instagram = new Andreyco\Instagram\Client(array(
        'apiKey' => 'a39d9504d62b4dbcbf5eae16acba0897',
        'apiSecret' => 'a9dc85acdc684e57952cbf519334e5bb',
        'apiCallback' => 'http://carolebamford.verb/int.php',
        'scope' => ['basic'],
    ));

    if (isset($_GET['code'])) :
        $code = $_GET['code'];
        $data = $instagram->getOAuthToken($code);

        // Now, you have access to authentication token and user profile
        echo 'Your username is: ' . $data->user->username;
        echo 'Your access token is: ' . $data->access_token;

    else:

        echo "<a href='{$instagram->getLoginUrl()}'>Login with Instagram</a>";

    endif;

} catch (\Andreyco\Instagram\Exception\InvalidParameterException $e) {
}

